# .bashrc
echo "Executing .bashrc"

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions

function if_
{
    if (( $1 )); then
        echo $2
    fi
}

function ,
{
    ls -lG $* 
}

function ,,
{
    ls -G $* 
}

function +
{
    local exe="$FUNCNAME"
    local usage="$exe <dir>"

    case $# in
        1) pushd $1
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
            ;;
    esac
}

function -
{
    local exe="$FUNCNAME"
    local usage="$exe <dir>"

    case $# in
        0) popd
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
            ;;
    esac
}

function ?
{
    local exe="$FUNCNAME"
    local usage="$exe"

    case $# in
        0) dirs
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
            ;;
    esac
}

function h
{
    local exe="$FUNCNAME"
    local usage="$exe"

    case $# in
        0) history | less +G
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
            ;;
    esac
}

function ..
{
    local exe="$FUNCNAME"
    local usage="$exe"

    case $# in
        0) cd ..
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
    esac
}

function clean
{
    make clean
}

function md
{
    local exe="$FUNCNAME"
    local usage="$exe <directory>"

	case $# in
		1) mkdir -p $1 && cd $1
			;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
			;;
    esac
}

function dev
{
    ssh -t rjones@bc-ld4-05.dev "export NOMAD_TOKEN=$(mnomad dev print-token); bash"
}

function path
{
    local exe="$FUNCNAME"
    local usage="$exe"

	case $# in
		0) for path in $(echo $PATH | tr ':' ' '); do
                echo $path
            done
			;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
			;;
    esac
}

function prepend
{
    local exe="$FUNCNAME"
    local usage="$exe"

	case $# in
		2) local result="$1"
            for path in $(echo ${2} | tr ':' ' '); do
                if [[ "$path" != "$1" ]]; then
                    result="$result:$path"
                fi
            done
            echo "$result"
			;;
        *) echo -e "Usage: $usage <dir> <path var>" 1>&2 && return 1
			;;
    esac
}

#
#   Similar to xargs, but spawns in the background and pauses for one
#   second between each spawn, largely because my primary use-case for
#   diffing files in bcompare doesn't work without the pause.
#
#   hg log -r . -T {files} | xbg hg bc -r -2 
#
function xbg
{
    local cmd="$@"
    sed 's/[ \t]\+/\n/g' | awk "{system( \"( $cmd \" \$0 \" & sleep 1 )\" ) }"
}

function xargs
{
    /usr/bin/xargs -n 1 "$@"
}

#
#   The last two components of the path, with the removed head
#   substituted with "..."
#
function briefpwd
{
    pwd | sed -E 's,.+/([^/]+/[^/]+),.../\1,'
}
 
function mkboosttags
{
    local exe="$FUNCNAME"
    local usage="$exe [-d] [-h] [-v]"

    local -i debug=0
    local -i showhelp=0
    local -i verbose=0
    local -i OPTIND

    local boostroot=/fs/sdb/proj/inferno/copyleft/linux/boost_1_49_0/include
    local tagfile=~/.rgj/boosttags

    while getopts ":dhv" opt; do
        case $opt in 
            d) debug=1
                ;;
            h) showhelp=1
                ;;
            v) verbose=1
                ;;
            \?)
                echo -e "Usage: ${usage}" 1>&2 && return 1
                ;;
        esac
    done

    shift $(($OPTIND-1))

    if (( showhelp )); then
        echo "Generate ctags for LATEST pubgen (all facilities)"
        echo "Usage: $usage"
        echo "    -d debug, don't execute"
        echo "    -h print this help text, and exit"
        echo "    -v verbose"
        return 0
    fi

    case $# in
        0)
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
    esac

#   
#   exctags is UniversalCtags from http://docs.ctags.io/en/boostroot/index.html
#
#   Universal  Ctags  is  used  because  the system ctags command does not
#   recognise C++ 'final' class declaration.
#
#   local cmd="ctags --recurse --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -a -f $tagfile ${boostroot}"
    local cmd="exctags --recurse --sort=1 --c++-kinds=+p --fields=+iaS --extras=+q --language-force=C++ -f ${tagfile} ${boostroot}"
    (( verbose )) && echo "${cmd}..."
    (( ! debug )) && eval "${cmd}"
}

function mkcxxtags
{
    local exe="$FUNCNAME"
    local usage="$exe [-h]"

    local -i showhelp=0
    local -i OPTIND

    while getopts ":h" opt; do
        case $opt in 
            h) showhelp=1
                ;;
            \?)
                echo -e "Usage: ${usage}" 1>&2 && return 1
                ;;
        esac
    done

    shift $(($OPTIND-1))

    if (( showhelp )); then
        echo "Generate ctags in cwd into cxxtags.vi"
        echo "Usage: $usage"
        echo "    -h print this help text, and exit"
        return 0
    fi

    case $# in
        0)
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
    esac
#
#    The anatomy of the ctags command
#
#    set -x          Enable tracing (to show the ctags cmd on execution)
#    --recurse       into directories below
#    --sort          sort tags by tag name (reqd for older vi's)
#    --kinds-c++     +p, add prototypes tags
#    --field         +i, include inheritance information in extended tags fmt
#                    +a, include access information in extended tags fmt
#                    +S, include signature information in extended tags format
#    --extras        +q, include a class or namespace qualified entry for the tag
#    --languages
#    -f              output file, defaults to 'tags'
#
    ( set -x; ctags --recurse --sort=yes --kinds-c++=+p --fields=+iaS --extras=+q --languages=C++ -f cxxtags.vi $PWD )
}

function mkpytags
{
    local exe="$FUNCNAME"
    local usage="$exe [-h]"

    local -i showhelp=0
    local -i OPTIND

    while getopts ":h" opt; do
        case $opt in 
            h) showhelp=1
                ;;
            \?)
                echo -e "Usage: ${usage}" 1>&2 && return 1
                ;;
        esac
    done

    shift $(($OPTIND-1))

    if (( showhelp )); then
        echo "Generate ctags in cwd into pytags.vi"
        echo "Usage: $usage"
        echo "    -h print this help text, and exit"
        return 0
    fi

    case $# in
        0)
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
    esac
#
#    The anatomy of the ctags command
#
#    set -s          Enable tracing (to show the ctags cmd on execution)
#    --recurse       into directories below
#    --sort          sort tags by tag name (reqd for older vi's)
#    --field         +l, include language of the source fine in extended tags fmt
#    --languages
#    -f              output file, defaults to 'tags'
#
    ( set -x; ctags --recurse --sort=yes --fields=+l --languages=python -f pytags.vi )
}


function log
{
#	less options:
#      -S truncate rather then wrap long lines
#      -R use raw ascii ctl sequences 
#
#   git log --graph --abbrev-commit --decorate --date=relative --all --color=always | less -RS
    git log --graph --oneline --decorate --all --color=always | less -RS
}

function branch
{
    git branch --show-current
}

function branches
{
    git branch | less
}

#
# realpath requires "brew install coreutils"
#
function xvim {
    xargs realpath -m --relative-to=. | sort -u | xargs -o -n 100 vim -p "$@"
}

function renvar
{
    local exe="$FUNCNAME"
    local usage="$exe [-h] <oldname> <newname>"

    local -i showhelp=0
    local -i OPTIND

    while getopts ":h" opt; do
        case $opt in 
            h) showhelp=1
                ;;
            \?)
                echo -e "Usage: ${usage}" 1>&2 && return 1
                ;;
        esac
    done

    shift $(($OPTIND-1))

    if (( showhelp )); then
        echo "Rename a variable everywhere"
        echo "Usage: $usage"
        echo "    -h print this help text, and exit"
        return 0
    fi

    case $# in
        2)  oldname=$1
			newname=$2
            ;;
        *) echo -e "Usage: $usage" 1>&2 && return 1
    esac
	for f in $(find src/lib -type f -exec grep -qw ${oldname} {} \; -print); do
		sed -i -E "s/${oldname}/${newname}/g" $f
	 done
}
#
#   Ansi escape sequences for these text effects.
#
function colour-non          { echo -e '\033[0m'; }
function colour-white        { echo -e '\033[1;37m'; }
function colour-black        { echo -e '\033[0;30m'; }
function colour-blue         { echo -e '\033[0;34m'; }
function colour-light-blue   { echo -e '\033[1;34m'; }
function colour-green        { echo -e '\033[0;32m'; }
function colour-light-green  { echo -e '\033[1;32m'; }
function colour-cyan         { echo -e '\033[0;36m'; }
function colour-light-cyan   { echo -e '\033[1;36m'; }
function colour-red          { echo -e '\033[0;31m'; }
function colour-light-red    { echo -e '\033[1;31m'; }
function colour-purple       { echo -e '\033[0;35m'; }
function colour-light-purple { echo -e '\033[1;35m'; }
function colour-brown        { echo -e '\033[0;33m'; }
function colour-yellow       { echo -e '\033[1;33m'; }
function colour-gray         { echo -e '\033[0;30m'; }
function colour-light-gray   { echo -e '\033[0;37m'; }

function title               { echo -ne "\033]0;${1}\007\c"; }

[[ -r ~/.dircolors ]] && eval "$(dircolors ~/.dircolors)"

set -o vi

# Updates PATH for the Google Cloud SDK.
if [ -f '/Users/rjones/local/google-cloud-sdk/path.bash.inc' ]; then . '/Users/rjones/local/google-cloud-sdk/path.bash.inc'; fi

# Shell command completion for gcloud.
if [ -f '/Users/rjones/local/google-cloud-sdk/completion.bash.inc' ]; then . '/Users/rjones/local/google-cloud-sdk/completion.bash.inc'; fi

# Git autocomplete
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi

#
# One of my Github tokens - Githuv won't tell you these so don't lose it!
#
export GH_TOKEN=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

#
# Nexus credentials supplied by Chris Norman, stored in LastPass.
#
export NEXUS_USER=rjones
export NEXUS_PASS=OtWkJf8DG4Ao

export SVN_EDITOR=vim
export EDITOR=vim
export PS1='\[$(colour-green)\]\u\[$(colour-non)\]@\[$(colour-yellow)\]\h\[$(colour-non)\]:\[$(colour-cyan)\]$(briefpwd) \[$(colour-non)\](\j)[\!] '
export LSCOLORS="bxfxcxdxbxegedabagacad"

. "$HOME/.cargo/env"
export PATH=$(prepend ~/bin $PATH)

