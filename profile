# .profile

echo "Executing .profile"

if [ -f ~/.bashrc ]; then
. ~/.bashrc
fi

# . "$HOME/.cargo/env"

if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi

