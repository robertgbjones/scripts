"
" .vimrc
"
"
" so ~/.vundle

" -- SnakeCase -----------------------------------------------

function! SnakeCase(s)
    let result = a:s
    let result = substitute(result, '^\([A-Z]\)', '\l\1', '')
    let result = substitute(result, '\([A-Z]\)', '_\l\1', 'g')
    return result
endfunction

" -- CamelCase -----------------------------------------------

function! CamelCase(s)
    let result = a:s
    let result = substitute(result, '^\([a-z]\)', '\u\1', '')
    let result = substitute(result, '_\([a-z]\)', '\u\1', 'g')
    return result
endfunction

" -- TwiddleCase ---------------------------------------------

function! TwiddleCase(s)
    let result = a:s
    if result ==# CamelCase(result)
        let result = SnakeCase(result)
    elseif result ==# SnakeCase(result)
        let result = CamelCase(result)
    endif
    return result
endfunction

" -- Canonicalise --------------------------------------------

function! Canonicalise(s)
	let result = a:s
	let result = substitute(result, '^[bilups]*', '', '')
	let result = substitute(result, '\([a-z]\)\([A-Z]\)', '\1_\2', 'g')
	let result = substitute(result, '.*', '\L&', '')
	return result
endfunction

function! SafeCanonicalise() range
	let tgt = substitute( @/, '\\<\(.*\)\\>','\1', '' )
    let tgt = Canonicalise(tgt)
    let save_cursor = getcurpos()
    call cursor(a:firstline, 1)
    if search( tgt ) != 0
        echoerr tgt . ' already exists'
        call setpos('.', save_cursor)
    endif
    call setpos('.', save_cursor)
    return
endfunction

command! -range=% SafeCanonicalise <line1>,<line2>call SafeCanonicalise()

" -- Comment -------------------------------------------------

function! Comment() range
    execute a:firstline . "," . a:lastline 's/^/\/\/ /'
    " execute a:firstline . "," . a:lastline 's/^/# /'
endfunction

command! -range=% Comment <line1>,<line2>call Comment()

" -- Uncomment -----------------------------------------------

function! Uncomment() range
    execute a:firstline . "," . a:lastline 's/^\/\/ //'
    " execute a:firstline . "," . a:lastline 's/^# //'
endfunction

command! -range=% Uncomment <line1>,<line2>call Uncomment()

" -- Escape --------------------------------------------------

function! Escape() range
    " Discover longest line in the range.
    let maxlen = 0

    let cur = a:firstline
    while cur <= a:lastline
        let line = getline(cur)
        let line = substitute(line, '[\t \\]*$', '', '' )
        let len = strdisplaywidth( line )
        if len > maxlen
            let maxlen = len
        endif
        call setline( cur, line )
        let cur = cur + 1
    endwhile

    " make every line that long.
    let cur = a:firstline
    while cur <= a:lastline
        let line = getline(cur)
        let line = substitute( line, '$', repeat( ' ', maxlen - strdisplaywidth( line ) ) . ' \\', '' )
        call setline( cur, line )
        let cur = cur + 1
    endwhile
endfunction

command! -range=% Escape <line1>,<line2>call Escape()

" -- Unescape ------------------------------------------------

function! Unescape() range
  execute a:firstline . "," . a:lastline . 's/\s*\\\s*$//'
endfunction

command! -range=% Unescape <line1>,<line2>call Unescape()

" -- Spread --------------------------------------------------

function! Spread() range
  " Box, parens & brace, both left and right.
  execute a:firstline . "," . a:lastline . 'g/[({[]\S\@=/s//& /gc'
  execute a:firstline . "," . a:lastline . 'g/\S\@<=[])}]/s// &/gc'
  " Address of, avoiding '&&' and '&='
  execute a:firstline . "," . a:lastline . 'g/\(&\|\s\)\@<!&/s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/&\(,\|=\|&\|\s\)\@!/s//& /gc'
  " '+', avoiding '+=', and '++'
  execute a:firstline . "," . a:lastline . 'g/\(+\|\s\)\@<!+/s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/+\(=\|+\|\s\)\@!/s//& /gc'
  " '-', avoiding '-=', and '->', '--'
  execute a:firstline . "," . a:lastline . 'g/\(-\|\s\)\@<!-/s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/-\(>\|=\|-\|\s\)\@!/s//& /gc'
  " '*', avoiding '**', '*.' and '*='
  execute a:firstline . "," . a:lastline . 'g/\(\*\|\s\)\@<!\*/s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/\*\(,\|=\|\*\|\s\|>\)\@!/s//& /gc'
  " '/', avoiding '/='
  execute a:firstline . "," . a:lastline . 'g/\(\S\&[^/]\)\@<=\//s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/\/\(\s\|\/\|=\)\@!/s//& /gc'
  " ','
  execute a:firstline . "," . a:lastline . 'g/,\S\@=/s//& /gc'
  " '!', avoiding '!='
  execute a:firstline . "," . a:lastline . 'g/!\(\s\|=\)\@!/s//& /gc'
  " '=', avoiding ...
  execute a:firstline . "," . a:lastline . 'g/\(\s\|[<>=!+*/^%&|-]\)\@<!=/s// &/gc'
  execute a:firstline . "," . a:lastline . 'g/=\@!=/s//& /gc'
endfunction

command! -range=% Spread <line1>,<line2>call Spread()

" -- Blockify ------------------------------------------------

function! Blockify() range
  execute a:firstline . "," . a:lastline . 's/[(,]\s*/& /g'
endfunction

command! -range=% Blockify <line1>,<line2>call Blockify()

" -- Unspread --------------------------------------------------

function! Unspread() range
  " Shrink embedded whitespace
  execute a:firstline . "," . a:lastline .  's/\S\zs\(\t\|\s\{2,}\)\ze\S/ /g'
endfunction

command! -range=% Unspread <line1>,<line2>call Unspread()

" -- Align ---------------------------------------------------

function! Align(s, ...) range
    let occurences_to_skip = 0
    let s = a:s
    if a:0 > 0
        let occurence = str2nr( a:1, 10 )
        if occurence > 1
            let occurences_to_skip = occurence - 1
        endif
    endif
    if occurences_to_skip == 0
        execute a:firstline . "," . a:lastline . '!sed ''s/' . s . '/@&/'' | column -s ''@'' -t | sed ''s/  \(' . s . '\)/\1/'''
    elseif occurences_to_skip > 0
        execute a:firstline . "," . a:lastline . '!sed ''s/^\(\([^' . s . ']*' . s . '\)\{' . occurences_to_skip . '\}[^' . s . ']*\)\(' . s . '\)/\1@\3/'' | column -s ''@'' -t | sed ''s/  \(' . s . '\)/\1/'''
    endif
endfunction

command! -range -nargs=+ Align <line1>,<line2>call Align(<f-args>)

" ------------------------------------------------------------

function! Resetpath()
  execute "set path=,,"
  execute "set path+=."
  execute "set path+=.."
  execute "set path+=../include"
  execute "set path+=../src"
  " execute "set path+=../.."
  execute "set path+=/usr/include"
endfunction

command! Resetpath call Resetpath()

" ------------------------------------------------------------

function! Setpath()
  execute "set path=".system( "make include" )
endfunction

command! Setpath call Setpath()

" ------------------------------------------------------------

" command! Std :set isk=@,48-57,_,192-255

let g:highlighting = 0

function! Highlighting()
  if g:highlighting == 1 && @/ =~ '^\\<'.expand('<cword>').'\\>$'
    let g:highlighting = 0
    return ":silent nohlsearch\<CR>"
  endif
  let @/ = '\<'.expand('<cword>').'\>'
  let g:highlighting = 1
  return ":silent set hlsearch\<CR>"
endfunction

function! FileToken(filename)
    return system( "echo '".a:filename."' | sed 's,.*/,,' | tr '[:lower:].-' '[:upper:]__'" )
endfunction

function! StdKeyMapping()
    source ~/.stdvimkeys
endfunction

function! MacKeyMapping()
    source ~/.macvimkeys
endfunction

function! BatKeyMapping()
    source ~/.batvimkeys
endfunction

" ------------------------------------------------------------

set cindent
set expandtab ts=4 sw=4 nu ai si nowrap
set incsearch
set laststatus=2
set mps+=<:>
set nocst
set noswapfile
set previewheight=30
set scrolloff=5
set showtabline=2
set sidescroll=1
set sidescrolloff=10
set statusline=%F%m%r%h%w\%<%=\[%{g:displaycrate}\]\[%{g:displayaction}\]\[%{g:displaytags}\]\ [%4l\ of\ %L\ \(%p%%\),\ col\ %v\]
" set statusline=%F%m%r%h%w\ [format=%{&ff}]\ [type=%y]\ [%4l\ of\ %L\ \(%p%%\),\ col\ %v\]
set switchbuf=useopen,usetab
set tabpagemax=20
set wildmenu

" let Tlist_Auto_Highlight_Tag=
" let Tlist_Auto_Open=
" let Tlist_Auto_Update=
let Tlist_Close_On_Select=1
" let Tlist_Compact_Format=
" let Tlist_Ctags_Cmd=
" let Tlist_Display_Prototype=
" let Tlist_Display_Tag_Scope=
" let Tlist_Enable_Fold_Column=
" let Tlist_Exit_OnlyWindow=
" let Tlist_File_Fold_Auto_Close=
let Tlist_GainFocus_On_ToggleOpen=1
" let Tlist_Highlight_Tag_On_BufEnter=
let Tlist_Inc_Winwidth=0
" let Tlist_Max_Submenu_Items=
" let Tlist_Max_Tag_Length=
" let Tlist_Process_File_Always=
let Tlist_Show_Menu=1
" let Tlist_Show_One_File=
" let Tlist_Sort_Type=
" let Tlist_Use_Horiz_Window=
let Tlist_Use_Right_Window=1
" let Tlist_Use_SingleClick=
" let Tlist_WinHeight=
let Tlist_WinWidth=80

let git=$GIT

"
" Fully populated tags facility requires stdlib and boost tags. Stdlib tags
" can be generated from modified libstdc++ headers, see
"
"      http://www.vim.org/scripts/script.php?script_id=2358
"
" then (adjust paths to suit, and see mktags)
"
" ctags --recurse --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f ~/.vimtags/stdlib /home/rjones/Dropbox/stdlib
" ctags --recurse --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f ~/.vimtags/boost-1.47 /home/rjones/git/prod/VendorAPIs/boost/boost_1_47_0/include/boost
" ctags --recurse --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f ~/.vimtags/dag /home/rjones/git/prod/VendorAPIs/dag/include
" ctags --recurse --exclude='*.o' --sort=1 --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f ~/git/prod/tags /home/rjones/git/prod/Enterprise/VC
"
" execute "set tags=~/.rgj/idev/myrepo/tags.ln"
" execute "set tags+=~/.rgj/pubtags"
" execute "set tags+=~/.vimtags/stdlib"

function! MakePrompts( choices, currOpt)
    let prompts = ['Select:' ]
    let i = 1
    for opt in a:choices
        let indicator = " "
        if opt ==# a:currOpt
            let indicator = "*"
        endif
        call add( prompts, " " . indicator . " " . i . ") " . a:choices[ i - 1] )
        let i = i + 1
    endfor
    return prompts
endfunction

" -- Tags -----------------------------------------------------

function! Tags( )
    let result=[ ]
    " realtime-ledger
    call add( result,
            \    '~/blockchain/mercury/cxxtags.vi'
            \ . ',~/blockchain/lib-mercury-cpp-core/cxxtags.vi'
            \ . ',~/blockchain/service-nabu-realtime-ledger/cxxtags.vi'
            \ . ',~/local/rapidjson/cxxtags.vi'
            \ . ',~/blockchain/googletest/cxxtags.vi'
            \ )
    " risk-engine-v2
    call add( result,
            \    '~/blockchain/lib-mercury-cpp-core/cxxtags.vi'
            \ . ',~/blockchain/service-mercury-risk-engine-v2/cxxtags.vi'
            \ . ',~/blockchain/mercury/cxxtags.vi'
            \ . ',~/local/rapidjson/cxxtags.vi'
            \ . ',~/blockchain/googletest/cxxtags.vi'
            \ )
    " liquidation-engine
    call add( result,
            \   '~/blockchain/mercury/cxxtags.vi'
            \ . ',~/blockchain/lib-mercury-cpp-core/cxxtags.vi'
            \ . ',~/blockchain/service-mercury-margin-liquidation-engine/cxxtags.vi'
            \ . ',~/local/rapidjson/cxxtags.vi'
            \ . ',~/blockchain/googletest/cxxtags.vi'
            \ )
    " googletest
    call add( result, ',~/blockchain/googletest/cxxtags.vi' )

    "
    " Exposition...
    "    sed -n -e '1 {x; d}' -e 'H' -e '$ { x;s/\\n/,/g; p; }'
    "       -n                         - don't print the pattern buffer
    "       -e '1 {x; d}               - for line one, exchange hold and pattern spaces; delete pattern space
    "       -e 'H'                     - all lines, append pattern space to hold space, separating with newline
    "       -e '$ { x;s/\\n/,/g; p; }' - last line, exchange, replace all newlines with commas, and print
    "
    " call add( result, system( "find /home/robert/repos/thor-core -name 'rusty-tags.vi' 
    "            \ | sed -n -e '1 {x; d}' -e 'H' -e '$ { x;s/\\n/,/g; p; }'" ))
    return result
endfunction

function! DisplayTags( )
    let result=[ ]
    call add( result, 'realtime-ledger' )
    call add( result, 'risk-engine-v2' )
    call add( result, 'liquidation-engine' )
    call add( result, 'googletest' )
    " call add( result, 'thor-core' )
    return result
endfunction

function! SetTagsFor( index )
    let display=DisplayTags( )
    if len(display) > 0
        if a:index >= 0 && a:index < len(display)
            let index = a:index
        else
            let index = 0
        endif
        let g:displaytags=display[ index ]
        execute 'set tags=' . Tags( )[ index ]
    else
        let g:displaytags=""
        set tags=
    endif
endfunction

function! SelectTags( useDefault )
    let display=DisplayTags( )

    let choice=1
    if ! a:useDefault
        let choice=inputlist( MakePrompts( display, g:displaytags ) )
    endif
    if choice
        call SetTagsFor( choice - 1)
    endif
endfunction

function! QueryTags( )
    set tags
endfunction

call SetTagsFor( 1 )

" -- Crate ----------------------------------------------------

function! Crates( )
"     return [ '.' ]
"     return systemlist(
"             \ 'for d in $(find . -maxdepth 3 -type d -name src | sort ); do'
"             \ . ' realpath --relative-base=. ${d}/.. ;'
"             \ . 'done'
"             \ )
    return systemlist(
            \ '{ echo "."; for d in $(find . -maxdepth 3 -type f -name Cargo.toml ); do'
            \ . ' realpath --relative-base=. $(dirname ${d}) ;'
            \ . 'done; } | sort -u'
            \ )
endfunction

function! DisplayCrates( )
    return Crates()
endfunction

function! SetCrateFor( index )
    let display=DisplayCrates( )
    if len(display) > 0
        if a:index >= 0 && a:index < len(display)
            let index = a:index
        else
            let index = 0
        endif
        let g:displaycrate=display[ index ]
        let g:crate=Crates( )[ index ]
    else
        let g:displaycrate=""
        let g:crate=""
    endif
endfunction

function! SelectCrate( useDefault )
    let display=DisplayCrates( )
    if len(display) > 0
        let choice=1
        if ! a:useDefault
            let choice=inputlist( MakePrompts( display, g:displaycrate ) )
        endif
        if choice
            call SetCrateFor( choice - 1)
        endif
    else
        echom "No crates found"
        let g:displaycrate=""
        let g:crate=""
    endif
endfunction

call SetCrateFor( 29 )

" -- Action ---------------------------------------------------

"
" Take care that Actions() and DisplayActions() match!
"
function! Actions( )
    return [ 
        \   'make'
        \ , 'ninja -C fix/build'
        \ , 'ninja test -C fix/build'
        \ , 'cargo fmt && cargo build && cargo clippy && cargo test'
        \ , 'cargo build'
        \ , 'cargo run'
        \ , 'cargo clippy'
        \ , 'cargo fmt'
        \ , 'cargo test'
    \ ]
" Some extra actions, which syntactically can't be commented out in situ in the list above.
        " \ , 'cargo test -- --nocapture'
        " \ , 'cargo test funding_event_with_traders_with_positions_produces_funding_messages -- --nocapture'
        " \ , 'env RUST_BACKTRACE=1 cargo test'
endfunction

"
" Take care that Actions() and DisplayActions() match!
"
function! DisplayActions( )
    return [
        \   'make'
        \ , 'ninja -C fix/build'
        \ , 'ninja test -C fix/build'
        \ , 'all'
        \ , 'build'
        \ , 'run'
        \ , 'clippy'
        \ , 'fmt'
        \ , 'test'
    \ ]
" Some extra display actions, which syntactically can't be commented out in the list.
        " \ , 'test (show output)'
        " \ , 'test funding_event_with_traders_with_positions_produces_funding_messages'
        " \ , 'trace'
endfunction

function! SetActionFor( index )
    if a:index >= 0 && a:index < len(DisplayActions())
        let index = a:index
    else
        let index = 0
    endif
    let g:displayaction=DisplayActions( )[ index ]
    let g:action=Actions( )[ index ]
endfunction

function! SelectAction( useDefault )
    let display=DisplayActions( )

    let choice=1
    if ! a:useDefault
        let choice=inputlist( MakePrompts( display, g:displayaction ) )
    endif
    if choice
        call SetActionFor( choice - 1)
    endif
endfunction

call SetActionFor( 4 )

" -- Make ----------------------------------------------------

function! Make()
    if g:crate == "-"
        echom "Crate not set."
    else
        let &makeprg='(cd ' . g:crate . ' && ' . g:action . ') 2>&1'
        make
    endif
endfunction

" ------------------------------------------------------------

set autowrite
" set makeprg=gmake\ %<.o
set makeprg=make\ system
set cmdheight=2
set nocompatible
syntax enable

filetype plugin on

" set completeopt=menuone,menu,longest,preview
set completeopt=menuone,menu,longest

hi MatchParen cterm=bold ctermfg=white ctermbg=blue

let g:netrw_liststyle=3
let g:alternateExtensions_h = "C,c,cxx,cpp,CPP,CXX,cc,CC"
let g:alternateExtensions_H = "c,C,cxx,cpp,CPP,CXX,cc,CC"
let g:alternateExtensions_c = "h,H"
let g:alternateExtensions_C = "h,H"
let g:alternateNoDefaultAlternate = 1
" let g:alternateSearchPath = '../src,../inc,../pubinc,../source,../pubgen,../gen,../include'

"
"   Auto Commands, by event.
"
" autocmd CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif

autocmd BufNewFile,BufRead *.cpp set syntax=cpp11

autocmd VimEnter * call MacKeyMapping()

" autocmd BufEnter * silent! lcd %:p:h
autocmd BufEnter * silent! syntax on

" autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/,$RUST_SRC_PATH/rusty-tags.vi

" autocmd BufWritePost .vimrc so ~/.vimrc
autocmd BufWritePost *.rs :silent! exec "!rusty-tags vi --quiet --start-dir=" . expand('%:p:h') . "&" | redraw!

"
"   References
"
" Rust Tags        - https://github.com/dan-t/rusty-tags
" Rust Errors etc. - https://github.com/rust-lang/rust.vim
" Lib std:: tags   - http://www.vim.org/scripts/script.php?script_id=2358
" Universal Ctags  - http://docs.ctags.io/en/latest/index.html
"

