SCRIPTDIR = $(HOME)/scripts
CONFIGDIR = $(HOME)/scripts

SCRIPTS  =             \
    git-bc             \
    git-brief          \
    gitdiff            \
    git-exists         \
    git-files          \
    gitmerge           \
    git-review         \
    git-reviewbranch   \
    git-reviewcommit   \
    git-reviewrevision \
    git-summary        \
    git-tree           \
    cmt                \
    mk-schema          \
    mk-pipeline        \
    mk-first-pipeline  \

CONFIGS =        \
    bashrc       \
    gitconfig    \
    macvimkeys   \
    profile      \
    stdvimkeys   \
    vimrc        \
    clang-format \

BIN = $(HOME)/bin

.PHONY: all configs scripts clean $(SCRIPTS) $(CONFIGS)

all: $(SCRIPTS) $(CONFIGS)

configs: $(CONFIGS)

scripts: $(SCRIPTS)

$(CONFIGS):
	@ [ -e $(CONFIGDIR)/$@ ] || { echo "make: $(CONFIGDIR)/$@ - not found." 1>&2 && false; }
	@ [ -h $(HOME)/.$@ -o ! -e $(HOME)/.$@ ] || { echo "make: $(HOME)/.$@ - exists and is not a link." 1>&2 && false; }
	( cd $(HOME) && rm -f .$@ && ln -s $(CONFIGDIR)/$@ .$@)

$(SCRIPTS):
	@ [ -d $(BIN) ] || { echo "make: $(BIN) - not found or not a directory." 1>&2 && false; }
	@ [ -e $(SCRIPTDIR)/$@ ] || { echo "make: $(SCRIPTDIR)/$@ - not found." 1>&2 && false; }
	@ [ -h $(BIN)/$@ -o ! -e $(BIN)/$@ ] || { echo "make: $(BIN)/$@ - exists and is not a link." 1>&2 && false; }
	( cd $(BIN) && rm -f $@ && ln -s $(SCRIPTDIR)/$@ )


